import os
import numpy as np
import cv2
import matplotlib.pyplot as plt

from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model


def read_and_process(list_of_images):
    """"
    Returns two arrays:
        x in an array of resized images
        y is an array of labels
    """
    X = []
    y = []

    nrows = 150
    ncolumns = 150

    for image in list_of_images:
        im = cv2.imread(image, cv2.IMREAD_COLOR)
        im2 = cv2.resize(im, (nrows, ncolumns), interpolation=cv2.INTER_CUBIC)
        X.append(im2)

        if 'dog' in image:
            y.append(1)
        elif 'cat' in image:
            y.append(0)

    return X, y


test_dir = '../data/validation'
test_imgs = ['../data/validation/{}'.format(i) for i in os.listdir(test_dir)]

model = load_model("model_keras.h5")
model.load_weights("model_weights.h5")
X_test, y_test = read_and_process(test_imgs[130:140])
x = np.array(X_test)
test_datagen = ImageDataGenerator(rescale=1./255)

i = 0
text_labels = []
plt.figure(figsize=(30, 20))
columns = 10
for batch in test_datagen.flow(x, batch_size=1):
    pred = model.predict(batch)
    if pred > 0.5:
        text_labels.append("dog")
    else:
        text_labels.append("cat")
    plt.subplot(5 / columns + 1, columns, i + 1)
    plt.title(text_labels[i])
    imgplot = plt.imshow(batch[0])
    i += 1
    if i % 10 == 0:
        break
plt.show()
