import os
import random
import gc
from datetime import time

import numpy as np
import cv2
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard


def read_and_process(list_of_images):
    """"
    Returns two arrays:
        x in an array of resized images
        y is an array of labels
    """
    X = []
    y = []

    nrows = 150
    ncolumns = 150
    channels = 3

    for image in list_of_images:
        im = cv2.imread(image, cv2.IMREAD_COLOR)
        im2 = cv2.resize(im, (nrows, ncolumns), interpolation=cv2.INTER_CUBIC)
        X.append(im2)

        if 'dog' in image:
            y.append(1)
        elif 'cat' in image:
            y.append(0)

    return X, y


train_dir = '../data/train'
test_dir = '../data/validation'

train_dogs = ['../data/train/{}'.format(i) for i in os.listdir(train_dir) if 'dog' in i]
train_cats = ['../data/train/{}'.format(i) for i in os.listdir(train_dir) if 'cat' in i]
test_imgs = ['../data/validation/{}'.format(i) for i in os.listdir(test_dir)]

train_imgs = train_dogs[:2000] + train_cats[:2000]
random.shuffle(train_imgs)

#for ima in train_imgs[0:3]:
#    img = mpimg.imread(ima)
#    imgplot = plt.imshow(img)
#    plt.show()

X, y = read_and_process(train_imgs)

# Visualisation de certaines images
#plt.figure(figsize=(20, 10))
#columns = 5
#for i in range(columns):
#    plt.subplot(5 / columns + 1, columns, i + 1)
#    imgplot = plt.imshow(X[i])
#plt.show()

# Conversion vers des listes vers des tableau numpy
X = np.array(X)
y = np.array(y)

print("Shape of train images is: ", X.shape)
print("Shape of labels is: ", y.shape)

# Separation des images en groupe d'entrainement et de validation
X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.20, random_state=2)

print("Shape of train images is:", X_train.shape)
print("Shape of validation images is:", X_val.shape)
print("Shape of labels is:", y_train.shape)
print("Shape of labels is:", y_val.shape)

# Clean de la memoire
del X
del y
gc.collect()

# Recupere la taille des donnees d'entrainement et de validation
ntrain = len(X_train)
nval = len(X_val)

# batch_size = nombre d'elements qui seront propages dans le reseau
batch_size = 32

# Mise en place du modele :
# (1) = Creation d'un modele permettant d'empiler les couches sequentiellement
# (2) = Creation de la premiere couche avec les informations que l'on souhaite:
#           - filter size [32]: Taile de la dimension de la sortie
#           - kernel_size [3, 3]: Hauteur et longueur de la fenetre de convolution 2D
#           - activation ['RELU']: Fonction d'activation
#           - input shape [150, 150, 3] : On ne la premiere dimension de 4000, qui est la taille du batch
# (3) = Ajout d'un MaxPool2D, pour reduire la taille de l'espace et ainsi les operations dans le reseau et aide
#       notamment pour l'overfitting
# (4) = Ajout d'une couche Flatten. Mise a plat des donnees dans un seul vecteur.
# (5) = Ajout d'une couche Dropout pour empecher les sur-ajustementsur les donnees d'entrainement
# (6) = La derniere couche a une taille de sortie de 1 et une fonction d'activation differente. On souhaite detecte si
#       l'image correspond a un chien ou un chat. On veut que notre modele retourne en sortie une probabilite de
#       certitude de determination d'une image chien ou chat, un score haut voulant dire que le classifieur pense qu'il
#       s'agit d'un chien, et a l'inverse d'un chat. La fonction sigmoid est parfaite car elle prend un ensemble de
#       nombres et retourne une probabilite de distribution entre 0 et 1.
model = models.Sequential()  # (1)
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(150, 150, 3)))  # (2)
model.add(layers.MaxPool2D((2, 2)))  # (3)
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Flatten())  # (4)
model.add(layers.Dropout(0.5))  # (5)
model.add(layers.Dense(512, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))  # (6)

# Previsualitation
model.summary()

# Compilation du modele
model.compile(loss='binary_crossentropy', optimizer=optimizers.RMSprop(lr=1e-4), metrics=['acc'])

# Utilisation du module ImageDataGenerator pour effectuer des fonctions importantes lors de l'envoi des images
# d'entraitementau modèle
# 1 - Normalisation la valeur des pixels
# 2 - Effectue des transformations aleatoires
# 3 - Uniquement normaliser pour set de validation
train_datagen = ImageDataGenerator(rescale=1./255,
                                   rotation_range=40,
                                   width_shift_range=0.2,
                                   height_shift_range=0.2,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True)
val_datagen = ImageDataGenerator(rescale=1./255)

# Creation des generateurs pour les images
# Appel a la methode flow() en passant les donnes et les etiquettes (entrainement + validation).
# Le parametre "batch size" informe le generateur de donnees de prendre uniquement un certain nombre d'images a la fois
train_generator = train_datagen.flow(X_train, y_train, batch_size=batch_size)
val_generator = val_datagen.flow(X_val, y_val, batch_size=batch_size)

# Entrainement du reseau avec la fit() avec les parametres suivants :
#    - Le nombre d'etapes par epoch. Ce informe notre modele du nombre d'image que l'on souhaite travailler avant de
#      mettre a jour le gradient sur notre fonction de perte.Un epoch correspond a un cycle a travers le set entier
#      d'entrainement
tensorboard = TensorBoard(log_dir="logs")
history = model.fit_generator(train_generator,
                              steps_per_epoch=ntrain // batch_size,
                              epochs=64,
                              validation_data=val_generator,
                              validation_steps=nval // batch_size,
                              callbacks=[tensorboard])

# Sauvegarde du modèle
model.save_weights('model_weights.h5')
model.save('model_keras.h5')
